# from flask import session, current_app, g
from flask import current_app
from flask import session,g
from functools import wraps
# 定义登陆装饰器,封装用户登陆数据
def user_login_data(view_func):
    @wraps(view_func)
    def wrapper(*args,**kwargs):
        # 0 从session中取出用户的user_id
        user_id = session.get("user_id")
        # 0.1通过USER_ID取出用户对象
        user = None
        if user_id:
            try:
                from info.models import User
                user = User.query.get(user_id)
            except Exception as e:
                current_app.logger.error(e)
        # 3.将user数据封装到G对象
        g.user = user

        return view_func(*args,**kwargs)
    return wrapper



