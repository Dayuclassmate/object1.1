from flask import Blueprint
# 初始化蓝图
# 创建蓝图对象
news_blue = Blueprint("news",__name__,url_prefix="/news")

#使用蓝图对象,装饰视图函数
from . import views
