from flask import abort
from flask import current_app, jsonify
from flask import render_template
from flask import session

from info.models import News, User
from info.modules import news
from info.utils.response_code import RET
from . import news_blue
# 10.新闻详情展示(用户)
# 请求路径: /news/<int:news_id>
# 请求方式: GET
# 请求参数:news_id
# 返回值: detail.html页面, 用户data字典数据
@news_blue.route('/<int:news_id>')
def news_detail(news_id):
   # 0 从session中取出用户的user_id
   user_id = session.get("user_id")
   # 0.1通过USER_ID取出用户对象
   user = None
   if user_id:
       try:
            user= User.query.get(user_id)
       except Exception as e:
           current_app.logger.error(e)





   #  1.根据新闻编号,查询新闻对象
   try:
        news = News.query.get(news_id)
   except Exception as e:
       current_app.logger.error(e)
       return jsonify(errno=RET.DBERR,errmsg="获取新闻失败")

   if not news:
       abort(404)

   # 获取前6条热门新闻
   try:
        click_news = News.query.order_by(News.clicks.desc()).limit(6).all()
   except Exception as e:
       current_app.logger.error(e)
       # return  jsonify(errno=,errmsg="获取热门新闻失败")
    #将热门新闻的对象列表,转成字典列表
   click_news_list = []
   for item_news in click_news:
       click_news_list.append(item_news.to_dict())
    #5.判断用户是否收藏过该新闻
   is_collected = False
    #    用户需要登陆,并且该新闻在用户收藏过的新闻列表中
    #    if g.user:




    #  2.携带数据,渲染页面
   data = {"news_info":news.to_dict(),
           "user_info":user.to_dict() if user else "",
           "news_list":click_news_list,
           "is_collected":is_collected

           }


   return render_template("news/detail.html",data=data)
