import random
import re

from datetime import datetime
from flask import current_app, jsonify
from flask import json
from flask import make_response
from flask import session

from info import constants, db
from info import redis_store
from info.libs.yuntongxun.sms import CCP
from info.models import User
from info.utils.captcha.captcha import captcha
from info.utils.response_code import RET
from . import passport_blue
from flask import request

# 请求路径: /passport/logout
# 请求方式: POST
# 请求参数: 无
# 返回值: errno, errmsg
@passport_blue.route('/logout', methods=['POST'])
def logout():
    # 1清楚session信息
    session.pop("user_id",None)
    # 2 返回响应
    return jsonify(errno=RET.OK,errmsg="退出成功")


# 4.登陆用户
# 请求路径: /passport/login
# 请求方式: POST
# 请求参数: mobile,password
# 返回值: errno, errmsg
@passport_blue.route('/login', methods=['POST'])
def login():
    # 1.获取参数
    mobile = request.json.get('mobile')
    password = request.json.get('password')
    # 2.校验参数,为空校验
    if not all([mobile,password]):
        return jsonify(errno=RET.PARAMERR,errmsg='参数不全')
    # 3.通过用户手机号,到数据库查询用户对象
    try:
        user = User.query.filter(User.mobile ==mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg='获取用户失败')
    # 4.判断用户是否存在
    if not user:
        return jsonify(errno=RET.NODATA, errmsg='该用户不存在')
    # 5.校验用户密码是否正确
    if not user.check_passowrd(password):
        return jsonify(errno=RET.DATAERR, errmsg='密码错误')
    # 6.将用户信息保存到session中
    session["user_id"] = user.id
    # 6.1记录用户的最后登录时间
    user.last_login=datetime.now()
    # try:
    #     db.session.commit()
    # except Exception as e:
    #     current_app.logger.error(e)

    # 7.返回响应
    return jsonify(errno=RET.OK, errmsg='登陆成功')



# 3.注册用户
# 请求路径: /passport/register
# 请求方式: POST
# 请求参数: mobile, sms_code,password
# 返回值: errno, errmsg
# # 请求路径: /passport/sms_code

@passport_blue.route('/register', methods=['POST'])
def register():
    # 1.获取参数
    json_data = request.data
    dict_data = json.loads(json_data)
    mobile = dict_data.get("mobile")
    sms_code = dict_data.get("sms_code")
    password = dict_data.get("password")

    # 2.获取参数为空校验
    if not all([mobile,sms_code,password]):
        return jsonify(errno=RET.PARAMERR,errmsg="参数不全")
    # 3.手机号作为KEY 取出redis中的短信验证码
    try:
        redis_sms_code = redis_store.get("sms_code:%s"%mobile)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg="获取短信验证码失败")
    # 4.判断短信验证码是否过期
    if not redis_sms_code:
        return jsonify(errno=RET.NODATA, errmsg="短信验证码已过期")
    # 5.判断短信验码是否正确
    if sms_code != redis_sms_code:
        return jsonify(errno=RET.DATAERR, errmsg="短信验证码填写错误")
    # 6.删除短信验证码
    try:
        redis_store.delete("sms_code:%s"%mobile)
    except Exception as e :
        current_app.logger.error(e)
        return jsonify(errno=RET.DATAERR, errmsg="短信验证码删除失败")
    # 7.创建用户对象
    user = User()
    # 8.设置用户对象属性
    user.nick_name = mobile
    # user.password_hash =password
    user.password =password #加密处理
    user.mobile = mobile
    user.signature = "该用户什么都没写~~~~"
    # 9. 保存到数据库
    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="用户注册失败")
    # 10.返回响应
    return jsonify(errno= RET.OK,errmsg="注册成功")



    pass

# 请求方式: POST
# 请求参数: mobile, image_code,image_code_id
# 返回值: errno, errmsg
@passport_blue.route('/sms_code', methods=['POST'])
def sms_code():
    '''
    1.获取参数
   json_data = request.data
   dict_data = json.loads(json_data)
   mobile =dict_data.get("mobile")
   image_code=dict_data.get("image_code")
   image_code_id=dict_data.get("image_code_id")
    # 2.获取参数图片验证码
   # 从redis中取出图片
   redis_image_code = redis_store.get("image_code:%s"%image_code_id)
   # 和传递的图片验证码比较
   if image_code != redis_image_code:
       return jsonify(errno=10000,errmsg="图片验证码填写错误")
    # 3. 验证参数,和手机格式
   if not re.match("1[3-9]\d{9}",mobile):
       return jsonify(errno=20000, errmsg="手机号的格式错误1")
   # 4.发送短信,调用封装好的CCp
   ccp=CCP()
   result = ccp.send_template_sms('13206529850', ['666666', 5], 1)
   if result == -1:
       return jsonify(errno=30000, errmsg="手机号的格式错误")

   return jsonify(errno=0, errmsg="短信发送成功")
'''
    # 1.获取参数
    json_data = request.data
    dict_data = json.loads(json_data)
    mobile = dict_data.get("mobile")
    image_code = dict_data.get("image_code")
    image_code_id = dict_data.get("image_code_id")
    # 2.参数为空校验
    if not all([mobile,image_code,image_code_id]):
        return jsonify(errno=RET.PARAMERR,errmsg ="参数不全")
    # 3.校验手机格式
    if not re.match("1[3-9]\d{9}", mobile):
        return jsonify(errno=RET.PARAMERR, errmsg="手机号的格式错误1")

    # 4.获取参数图片验证码,从redis中取出图片
    try:
        redis_image_code = redis_store.get("image_code:%s" % image_code_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg="操作redis失败")

    # 5判断图片验证码是否过期
    if not redis_image_code:
        return jsonify(errno=RET.NODATA,errmsg="图片验证码已过期")
    # 6判断图片验证码是否正确
    if image_code.upper() != redis_image_code.upper():
        return jsonify(errno=RET.DATAERR,errmsg="图片验证码填写错误")

    # 7删除redis中的图片验证码
    try:
        redis_store.delete("image_code:%s" % image_code_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg="删除redis图片验证码失败")
    # 8. 生成一个随机短信验证码,调用CCP发送短信,判断否发送成功
    sms_code = "%06d"%random.randint(0,999999)
    current_app.logger.debug("短信验证码是 = %s" %sms_code)
    # ccp = CCP()
    # result = ccp.send_template_sms(mobile, [sms_code, constants.SMS_CODE_REDIS_EXPIRES/60], 1)
    # if result == -1:
    #     return jsonify(errno=RET.DATAERR, errmsg="短信发送失败")
    # 9将短信保存到redis中
    try:
        redis_store.set("sms_code:%s" % mobile, sms_code,constants.SMS_CODE_REDIS_EXPIRES)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg="图片验证码保存到redis失败")
    # 10 返回响应
    return jsonify(errno=RET.OK, errmsg="短信发送成功")






    # # 5和传递的图片验证码比较
    # if image_code != redis_image_code:
    #     return jsonify(errno=10000, errmsg="图片验证码填写错误")
    #     # 3. 验证参数,和手机格式
    # if not re.match("1[3-9]\d{9}", mobile):
    #     return jsonify(errno=20000, errmsg="手机号的格式错误1")
    # # 4.发送短信,调用封装好的CCp
    # ccp = CCP()
    # result = ccp.send_template_sms('13206529850', ['666666', 5], 1)
    # if result == -1:
    #     return jsonify(errno=30000, errmsg="手机号的格式错误")
    #
    # return jsonify(errno=0, errmsg="短信发送成功")



#功能 获取图片验证码
@passport_blue.route('/image_code')
def image_code():
    # 获取前段传递的参数
    cur_id =request.args.get("cur_id")
    pre_id = request.args.get("pre_id")

    # 调用generate_captcha获取图片的验证码编号,验证码值,图片
    name, text, image_data = captcha.generate_captcha()
    # 将图片验证码保存到redis
    try:
        redis_store.set("image_code:%s"%cur_id,text,constants.IMAGE_CODE_REDIS_EXPIRES)

        if pre_id:
            redis_store.delete("image_code:%s"%pre_id)
    except Exception as e:
        current_app.logger.error(e)
        return "图片验证码操作失败"
    # 返回图片
    response = make_response(image_data)
    response.headers['Content-Type'] = 'image/png'
    return response