from flask import current_app, jsonify
from flask import render_template
from flask import request
from flask import session

from info.models import User, News, Category
from info.modules.index import index_blue
import logging

from info.utils.response_code import RET

# 9.首页新闻列表
# 请求路径: /newslist
# 请求方式: GET
# 请求参数: cid,page,per_page
# 返回值: data数据
@index_blue.route('/newslist')
def newslist():
    # 1.获取参数
    cid =request.args.get("cid","1")
    page = request.args.get("page", "1")
    per_page = request.args.get("per_page", "1")
    # 2.参数类型转换
    try:
        page = int(page)
        per_page = int(per_page)
    except Exception as e:
        current_app.logger.error(e)
    # 3.分页查询
    try:
        """
        # 判断新闻的分类是否为1
        if cid == "1":
            painate = News.query.filter().order_by(News.create_time.desc()).paginate(page,per_page,False)
        else:
            painate = News.query.filter(News.category_id == cid).order_by(News.create_time.desc()).paginate(page,per_page,False)
        """
        """
        filters = ""
        if cid != "1":
            filters = (News.category_id == cid)

        painate = News.query.filter(filters).order_by(News.create_time.desc()).paginate(page, per_page, False)
        """
        filters = []
        if cid != "1":
            filters.append(News.category_id == cid)

        painate = News.query.filter(*filters).order_by(News.create_time.desc()).paginate(page, per_page, False)

    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg="获取新闻失败")
    # 4.获取到分页对象中的属性,总页数,当前页,当前页的列表对象
    totalPage = painate.pages
    currenPage = painate.pages
    items = painate.items
    # 5.将对象列表转成字典
    new_list =[]
    for news in items:
        new_list.append((news.to_dict()))
    # 6.携带数据返回响应
    return jsonify(errno = RET.OK,errmsg ='获取新闻成功',totalPage=totalPage,currenPage=currenPage,newsList=new_list)



@index_blue.route('/',methods=["GET","POST"])
def show_index():
    # 1获取用户的登录信息
    user_id = session.get("user_id")
    # 2.通过user_id取出用户对象
    user =None
    if user_id:
        try:
            user = User.query.get(user_id)
        except Exception as e:
            current_app.logger.error(e)
    # 3.1查询热门新闻,根据点击量查询前十条新闻
    try:
        news = News.query.order_by(News.clicks.desc()).limit(10).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg="获取新闻失败")
    # 4.1将新闻对象列表转成,字典列表
    news_list = []
    for item in news:
        news_list.append(item.to_dict())

    # 5.查询所有的分类数据
    try:
        categories= Category.query.all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR,errmsg='获取分类失败')
    # 6 将分类的对象列表转成,字典列表
    category_list =[]
    for category in categories:
        category_list.append(category.to_dict())

    # 3.拼接用户数据渲染页面
    data ={
    #如果user有值返回左边的内容,否则返回右边的值
        "user_info":user.to_dict() if user else "",
        "news_list":news_list,
        "category_list":category_list
    }
    return render_template("news/index.html", data=data)
    # logging.debug("输入调试信息")
    # logging.info("输入详细信息")
    # logging.warning("输入警告信息")
    # logging.error("输入错误信息")
    #     有无分割线的区别
    # current_app.logger.debug("输入调试信息2")
    # current_app.logger.info("输入详细信息2")
    # current_app.logger.warning("输入警告信息2")
    # current_app.logger.error("输入错误信息2")


    return render_template("news/index.html")
# 处理网站logo
@index_blue.route('/favicon.ico')
def get_web_logo():
    return current_app.send_static_file('news/favicon.ico')
