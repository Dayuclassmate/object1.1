from datetime import timedelta
from logging.handlers import RotatingFileHandler

import logging
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from redis import StrictRedis
from flask_session import Session
from flask_wtf.csrf import CSRFProtect,generate_csrf
from config import config_dict


# 定义redis_store 变量
redis_store = None
# 定义db变量
db = SQLAlchemy()
# 工厂方法
def create_app(config_name):


    app = Flask(__name__)
    config = config_dict.get(config_name)
    log_file(config.LEVEL_NAME)
    # 加载配置对象
    app.config.from_object(config)
    # 创建sqlalchemy对象,关联APP
    db.init_app(app)

    global redis_store
    redis_store =StrictRedis(host=config.REDIS_HOST,port=config.REDIS_PORT,decode_responses=True)
    # 指定session初始化信息
    Session(app)
    # 设置CSRF保护
    CSRFProtect(app)
    # 将首页蓝图 index_blue注册到APP中
    from info.modules.index import index_blue
    app.register_blueprint(index_blue)

    # 将认证蓝图passport_blue,注册到app中
    from info.modules.passport import passport_blue
    app.register_blueprint(passport_blue)

    # 将新闻蓝图new_blue,注册到app中
    from info.modules.news import news_blue
    app.register_blueprint(news_blue)



    # 使用请求钩子拦截所有的请求,通过的在cookie中设置csrf_token
    @app.after_request
    def after_request(resp):
        # 调用系统方法,获取csrf_token
        csrf_token = generate_csrf()
        # csrf_token 设置到cookie中
        resp.set_cookie("csrf_token",csrf_token)
        # 返回响应
        return resp
    # from info.modules.passport import passport_blue
    # app.register_blueprint(passport_blue)
    print(app.url_map)
    return app

def log_file(LEVEL_NAME):
    # 设置日志的记录等级
    logging.basicConfig(level=LEVEL_NAME)  # 调试debug级
    # 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限
    file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024 * 1024 * 100, backupCount=10)
    # 创建日志记录的格式 日志等级 输入日志信息的文件名 行数 日志信息
    formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')
    # 为刚创建的日志记录器设置日志记录格式
    file_log_handler.setFormatter(formatter)
    # 为全局的日志工具对象（flask app使用的）添加日志记录器
    logging.getLogger().addHandler(file_log_handler)