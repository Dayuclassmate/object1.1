from datetime import timedelta

import logging
from redis import StrictRedis

# 设置配置信息(基类配置信息)
class Config(object):
    DEBUG = True
    SECRET_KEY = 'afsafs'
    # 数据库配置
    SQLALCHEMY_DATABASE_URI = "mysql://root:mysql@127.0.0.1:3306/info36"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN =True #每当改变数据内容之后,在试图函数结束的时候都会自动提交哦 亲
    # redis配置
    REDIS_HOST= "127.0.0.1"
    REDIS_PORT= 6379
    # session配置，session
    SESSION_TYPE='redis'
    SESSION_REDIS=StrictRedis(host=REDIS_HOST,port=REDIS_PORT)
    SESSION_USE_SIGNER =True
    PERMANENT_SESSION_LIFETIME = timedelta(days=2)

    LEVEL_NAME =logging.DEBUG

# 开发环境配置信息
class DevelopConfig(Config):
    pass
# 生产环境配置信息
class ProductConfig(Config):
   DEBUG =  False
   LEVEL_NAME = logging.ERROR

# 测试环境配置信息
class TestConfig(Config):
    pass

# 统一的访问入口
config_dict ={
    "develop":DevelopConfig,
    "product":ProductConfig,
    "test":TestConfig
}
